import 'package:flutter/material.dart';
import 'package:latihanapi/edit_todo.dart';
import 'package:latihanapi/models/TodoModel.dart';
import 'package:latihanapi/utils/api_client.dart';
import 'package:latihanapi/add_todo.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'latihan api',
      home: HomePage(),
      routes: <String, WidgetBuilder>{
        '/home': (_) => new HomePage(),
        '/addTodo': (_) => new AddTodo(),
        '/editTodo': (_) => new EditTodo(),
      },
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  _buildBody() {
    return FutureBuilder(
      future: api.getTodos(),
      builder: (BuildContext context, AsyncSnapshot<TodoModel> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
            break;
          case ConnectionState.done:
            if (snapshot.hasError) {
              return Center(
                child: Text(snapshot.error.toString()),
              );
            } else {
              if (snapshot.data.result.count != 0)
                return _buildList(snapshot.data.result);
              else
                return Center(child: Text("Tidak ada data"));
            }
            break;
          case ConnectionState.none:
            return Center(child: Text("Tidak ada koneksi"));
          default:
            return Center(child: Text("Tidak ada koneksi"));
        }
      },
    );
  }

  _buildList(ResultModel result) {
    return ListView.builder(
      itemCount: result.count,
      itemBuilder: (context, i) {
        Entries data = result.entries[i];
        return InkWell(
          onTap: () {
            Navigator.pushNamed(context, '/editTodo', arguments: {
              "todo": data,
            });
          },
          child: Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(child: Text(data.name)),
                Container(child: Text(data.description)),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Todo List"),
      ),
      body: _buildBody(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/addTodo');
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
