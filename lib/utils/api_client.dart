import 'dart:convert';
import 'package:latihanapi/models/TodoModel.dart';
import 'package:latihanapi/utils/network_utils.dart';

class ApiClient {
  NetworkUtil net;
  JsonDecoder _decoder = JsonDecoder();
  final String _baseUrl = "https://test-server-todo-api.herokuapp.com/";

  ApiClient() {
    net = new NetworkUtil(baseUrl: _baseUrl);
  }

  Future<TodoModel> getTodos() async {
    var result = await net.get("/");
    var json = _decoder.convert(result);
    return TodoModel.fromJson(json);
  }

  Future<dynamic> addTodos(data) async {
    var result = await net.post("/", body: data);
    return _decoder.convert(result);
  }
}

final ApiClient api = ApiClient();
