import 'package:flutter/material.dart';
import 'package:latihanapi/utils/api_client.dart';

class AddTodo extends StatefulWidget {
  AddTodo({Key key}) : super(key: key);

  @override
  _AddTodoState createState() => _AddTodoState();
}

class _AddTodoState extends State<AddTodo> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _name = TextEditingController();
  TextEditingController _description = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Todo"),
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Card(
                margin: EdgeInsets.only(bottom: 10),
                child: TextFormField(
                  maxLines: 1,
                  controller: _name,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Name cannot be empty !";
                    }
                  },
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 14, vertical: 12),
                    hintText: "Todo Name",
                  ),
                ),
              ),
              Card(
                margin: EdgeInsets.only(bottom: 10),
                child: TextFormField(
                  maxLines: 4,
                  controller: _description,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Description cannot be empty !";
                    }
                  },
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 14, vertical: 12),
                    hintText: "Description",
                  ),
                ),
              ),
              MaterialButton(
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    Map<String, dynamic> data = {
                      "name": _name.text,
                      "description": _description.text,
                    };
                    api.addTodos(data).then((result) {
                      if (result["code"] == 0) {
                        print("Todos Created");
                        Navigator.of(context).pop();
                      } else {
                        print(result["description"]);
                      }
                    });
                  }
                },
                color: Colors.lightBlue,
                height: 45,
                child: Text("Save Todo".toUpperCase(),
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
